import DS from 'ember-data';

export default DS.Model.extend({
  product: DS.belongsTo('product', { persistToServer: true }),
  rating: DS.attr('string', { persistToServer: true }),
  review: DS.attr('string', { persistToServer: true }),
  name: DS.attr('string', { persistToServer: true }),
  title: DS.attr('string', { persistToServer: true }),
  user: DS.belongsTo('user')
});