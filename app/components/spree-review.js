import Ember from 'ember';
export default Ember.Component.extend({
  review: {},
  actions: {
    submitReview: function(review, product) {
      var new_review = this.spree.store.createRecord('review', review);
      new_review.set('product', product);
      new_review.save().then(function (data) {
      }, function (errors) {
        alert(errors);
      });
    }
  }
});